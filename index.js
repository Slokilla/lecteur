const { app, BrowserWindow, ipcMain, dialog } = require('electron');
const fs = require('fs');

function createWindow () {
    // Cree la fenetre du navigateur.
    let win = new BrowserWindow({
        minWidth : 800,
        minHeight : 600,
        //show: false,
        webPreferences: {
            nodeIntegration: true
        }
    });

    // and load the index.html of the app.
    if(process.platform === "darwin")
        win.fullscreen = true;

    win.loadFile('index.html');
    //win.webContents.openDevTools();
    win.maximize();
    win.on('closed', () => {
        // Dé-référence l'objet window , normalement, vous stockeriez les fenêtres
        // dans un tableau si votre application supporte le multi-fenêtre. C'est le moment
        // où vous devez supprimer l'élément correspondant.
        win = null
    })
}

// Cette méthode sera appelée quand Electron aura fini
// de s'initialiser et sera prêt à créer des fenêtres de navigation.
// Certaines APIs peuvent être utilisées uniquement quand cet événement est émit.
app.on('ready', createWindow);


// Quitte l'application quand toutes les fenêtres sont fermées.
// app.on('window-all-closed', () => {
//     // Sur macOS, il est commun pour une application et leur barre de menu
//     // de rester active tant que l'utilisateur ne quitte pas explicitement avec Cmd + Q
//     if (process.platform !== 'darwin') {
//         app.quit()
//     }
// })

app.on('activate', () => {
    // Sur macOS, il est commun de re-créer une fenêtre de l'application quand
    // l'icône du dock est cliquée et qu'il n'y a pas d'autres fenêtres d'ouvertes.
    if (win === null) {
        createWindow()
    }
});

// Gestion de la fenêtre d'ouverture de fichier
ipcMain.on('openFile', function(event, path ="") {

    if (path === "") {
        path = dialog.showOpenDialogSync({
            title: "Fichier à lire",
            defaultPath: app.getPath("documents"), //à configurer
            buttonLabel: "Lire",
            filters: [
                {name: "Texte", extensions: ["txt"]}
            ],
            properties: ["openFile"] //on autorise l'ouverture de fichier
        });

        if (path) {
            try {
                fs.readFile(path.toString(), 'utf8', function (err, content) {
                    event.sender.send('actionReply', content, path);
                });
            }catch (e) {
                envent.sender.send("actionReply","","");
            }
        }
    } else {
        try {
            fs.readFile(path.toString(), 'utf8', function (err, content) {
                event.sender.send('actionReply', content, path);
            });
        }catch (e) {
            console.log(e);
        }
    }
});



//Gestion des fichiers récents
ipcMain.on('updateRecents', (event, filepath = "") =>{
    let jsonNom = "";
    let jsonPath ="";
    let listRecentHtml = {};
    console.log(filepath);

    //Lecture du fichier de donnée JSON
    try{
        fs.readFile("ressources/recents.json",function(err, content){
            if(err) throw(err);
            //On parcours les entrées
            JSON.parse(content, function(key,value){
                if (key === "nom"){
                    if (this.hasOwnProperty("nom")) {
                        jsonNom = this[key];
                    }
                }
                if (key === "path"){
                    if (this.hasOwnProperty("path")) {
                        jsonPath = this[key];
                    }
                }
                if (key === "date"){
                    if (this.hasOwnProperty("date")) {
                        listRecentHtml[this[key]] = {"path" : jsonPath, "name" : jsonNom};
                    }
                }
            });
            //Ajout du fichier dans les récents s'il n'en fait pas déjà partie;
            if(filepath !== ""){
                //console.log("file : " + filepath);
                // ret[0] ==> exists; ret[1] ==> date; ret[2] ==> filepath; ret[3] ==> fileName
                console.log(filepath);
                ret = addNewRecentFile(filepath.toString(),content);
                let exists= ret[0];
                let date =ret[1];
                let path = ret[2];
                let name = ret[3];
                //si le ichier est déjà dans la liste, on met à jour la date et on supprime l'ancien
                if (exists){
                    for(let key in listRecentHtml){
                        if(listRecentHtml.hasOwnProperty(key) && listRecentHtml[key]['path'] === path) {
                            listRecentHtml[date] = {'path' : path,'name' : name};
                            delete listRecentHtml[key];
                        }
                    }
                }
                //sinon on le rajoute simplement
                else{
                    listRecentHtml[date] = {"path" : path, "name" : name};
                }
            }
            event.sender.send('actionReply',JSON.stringify(orderList(listRecentHtml)))
        });
    }catch (e) {
        console.log(e);
    }
});

//Fonction qui ajoute un fichier récent à la liste JSON et renvoie son formatage en liste
function addNewRecentFile(filepath, content){
    console.log(filepath);
    let fileName = /\/[\s\S][^\/]*(?:.txt)$/gmi.exec(filepath).toString().substr(1); //regex pour récupérer le nom du fichier
    let countRecent = 0;
    let JSONFile= JSON.parse(content);
    let listJSON = JSONFile.listeRecent;
    let maxRecent = 10; // à redéfinir avec une fichier de config
    let exists = false ; //si un fichier est déjà dans la base, on retourne son rang
    let dateNow = Date.now();

    //on compte le nombre d'entrée, et on check si celle qu'on veut ajouter ne sont pas déjà dedans  pas déjà dedans
    for(var key in listJSON){
        if(listJSON.hasOwnProperty(key)){
            countRecent++; //on ajoute 1 au compteur
            //si le fichier qu'on ouvre dans les récents est déjà présent, on change sa date
            //Pour comparer on met filepath dans le même format que ce que renvoie stringify
            if(JSON.stringify(listJSON[key]["path"]) === JSON.stringify(filepath)){
                console.log('match');
                listJSON[key]["date"] =dateNow;
                exists = true;
            }
        }
    }
    if(!exists){
        if (countRecent < maxRecent) {
            listJSON[countRecent] = {nom: fileName, path: filepath.toString(), date: dateNow}; //le compte JSON commence à 0
        } else {
            //Au cas ou il y a déjà plus de récents que le souhaite l'utilisateur
        }
    }

    try {
        fs.writeFile("ressources/recents.json", JSON.stringify(JSONFile), err => {
            if (err) return err;
        });
    }catch (e) {
        console.log(e);
    }
    return [exists,dateNow,filepath,fileName];

}

function clearRecent(event){
    try {
        fs.writeFile("ressources/recents.json", JSON.stringify({"listeRecent": []}), err => {
            if (err) return err;
        });
    }catch (e) {
        console.log(e);
    }
    updateRecents(event);
}

//fonction qui range les récents du plus récent au plus vieux
function orderList(listItem){
    let orderedListItem = {};
    //on trie les clé (dates) du plus récent au plus vieux
    Object.keys(listItem).sort((a,b) => {return b-a}).forEach(function (key) {
        orderedListItem[key] = listItem[key];
    });
    return orderedListItem;
}