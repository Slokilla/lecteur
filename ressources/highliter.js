// Original JavaScript code by Chirp Internet: www.chirp.com.au
// Please acknowledge use of this code by including this header.
//Modifié par Samir Alicherif pour prendre en compte les fonctionnalités suivantes:
// - Surligner les espaces
// - Moduler la sensibilité à la casse
// -Support des accents

function Hilitor(id, tag)
{

    // private variables
    var targetNode = document.getElementById(id) || document.body;
    var hiliteTag = tag || "MARK";
    var skipTags = new RegExp("^(?:" + hiliteTag + "|SCRIPT|FORM|SPAN)$");
    var colors = ["#ff6", "#a0ffff", "#9f9", "#f99", "#f6f"];
    var wordColor = [];
    var colorIdx = 0;
    var caseSensitivity = false
    var matchRegExp = "";
    var openLeft = false;
    var openRight = false;

    // characters to strip from start and end of the input string
    var endRegExp = new RegExp('^[^\\s\\S]+|[^\\s\\S ]+$', "g");

    // characters used to break up the input string into words
    var breakRegExp = new RegExp('[^\\s\\S]+', "g");

    this.setEndRegExp = function(regex) {
        endRegExp = regex;
        return endRegExp;
    };

    this.setBreakRegExp = function(regex) {
        breakRegExp = regex;
        return breakRegExp;
    };

    this.setMatchType = function(type)
    {
        switch(type)
        {
            case "left":
                this.openLeft = false;
                this.openRight = true;
                break;

            case "right":
                this.openLeft = true;
                this.openRight = false;
                break;

            case "open":
                this.openLeft = this.openRight = true;
                break;

            default:
                this.openLeft = this.openRight = false;

        }
    };

    this.setRegex = function(input)
    {
        input = input.replace(endRegExp, "");
        input = input.replace(breakRegExp, "|");
        input = input.replace(/^\||\|$/g, "");
        if(input) {
            var re = "(" + input + ")";
            if(!this.openLeft) {
                re = "\\b" + re;
            }
            if(!this.openRight) {
                re = re + "\\b";
            }
            matchRegExp = caseSensitivity ? new RegExp(re) : new RegExp(re, "i");
            return matchRegExp;
        }
        return false;
    };

    this.getRegex = function()
    {
        var retval = matchRegExp.toString();
        retval = retval.replace(/(^\/(\\b)?|\(|\)|(\\b)?\/i$)/g, "");
        retval = retval.replace(/\|/g, " ");
        return retval;
    };

    //Gestion de la sensibilité à la casse
    this.setCaseSensitivity = function(isSensitive){
        caseSensitivity = !!isSensitive;
    };
    // recursively apply word highlighting
    this.hiliteWords = function(node)
    {
        if(node === undefined || !node) return;
        if(!matchRegExp) return;
        if(skipTags.test(node.nodeName)) return;
        //console.log(matchRegExp);

        if(node.hasChildNodes()) {
            for(var i=0; i < node.childNodes.length; i++)
                this.hiliteWords(node.childNodes[i]);
        }
        if(node.nodeType == 3) { // NODE_TEXT
            if((nv = node.nodeValue) && (regs = matchRegExp.exec(nv))) {
                if(!wordColor[regs[0].toLowerCase()]) {
                    wordColor[regs[0].toLowerCase()] = "#ff6";
                }

                var match = document.createElement(hiliteTag);
                match.appendChild(document.createTextNode(regs[0]));
                match.style.backgroundColor = wordColor[regs[0].toLowerCase()];
                match.style.color = "#000";

                var after = node.splitText(regs.index);
                after.nodeValue = after.nodeValue.substring(regs[0].length);
                node.parentNode.insertBefore(match, after);
            }
        }
    };

    // remove highlighting
    this.remove = function()
    {
        var arr = document.getElementsByTagName(hiliteTag);
        while(arr.length && (el = arr[0])) {
            var parent = el.parentNode;
            parent.replaceChild(el.firstChild, el);
            parent.normalize();
        }
    };

    // start highlighting at target node
    this.apply = function(input)
    {
        this.remove();
        //Version originale
        //if(input === undefined || !(input = input.replace(/(^\s+|\s+$)/g, ""))) {
        //Version prenant en compte les espaces à la fin et au début de la recherche
        if(input === undefined) {
            return;
        }
        if(this.setRegex(input)) {
            this.hiliteWords(targetNode);
        }
        return matchRegExp;
    };

}